package com.nespresso.sofa.recruitment.restaurant;

import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.exceptions.UnavailableDishException;
import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.parser.stock.SpaceStockParser;
import com.nespresso.sofa.recruitment.restaurant.parser.stock.StockParser;
import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;

public class Restaurant {

	Map<Ingeredient, Quantity> stock;
	StockParser stockParser = new SpaceStockParser();

	public Restaurant(String... stockInfo) {
		stock = stockParser.parse(stockInfo);
	}

	public Ticket order(String recipeInfo) {
		Ticket ticket = new Ticket();
		ticket.addRecipe(recipeInfo);
		if (ticket.canBeExecute(this.stock)) {
			return ticket;
		}
		throw new UnavailableDishException();
	}

	public Meal retrieve(Ticket ticket) {
		return ticket.getMeal();
	}

}
