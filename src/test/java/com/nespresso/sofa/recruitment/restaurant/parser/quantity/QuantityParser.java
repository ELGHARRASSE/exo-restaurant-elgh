package com.nespresso.sofa.recruitment.restaurant.parser.quantity;

import com.nespresso.sofa.recruitment.restaurant.models.Quantity;

public interface QuantityParser {

	public Quantity parse(String quantityInfo);
}
