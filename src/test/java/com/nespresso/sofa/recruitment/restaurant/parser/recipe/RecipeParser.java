package com.nespresso.sofa.recruitment.restaurant.parser.recipe;

import com.nespresso.sofa.recruitment.restaurant.Meal;

public interface RecipeParser {
	
	public Meal parse(String recipeInfo);

}
