package com.nespresso.sofa.recruitment.restaurant.parser.recipe;

import com.nespresso.sofa.recruitment.restaurant.Meal;
import com.nespresso.sofa.recruitment.restaurant.types.Recipe;

public class SpaceRecipeParser implements RecipeParser {

	@Override
	public Meal parse(String recipeInfo) {
		Meal mealResult=new Meal();
		
		String elements[]=recipeInfo.split(" ");
		Integer quantity=Integer.valueOf(elements[0]);
		StringBuilder recipeName = buildRecipeName(elements);
		Recipe recipe=Recipe.getRecipeByName(recipeName.toString());
		
		mealResult.addRecipeWithQuantity(recipe,quantity);
		
		return mealResult;
	}
	
	
	private StringBuilder buildRecipeName(String[] elements) {
		StringBuilder recipeName=new StringBuilder();
		for(int i=1;i<elements.length;i++){
			recipeName.append(elements[i]);
			if(i!=elements.length-1){
				recipeName.append(" ");
			}
		}
		return recipeName;
	}
}
