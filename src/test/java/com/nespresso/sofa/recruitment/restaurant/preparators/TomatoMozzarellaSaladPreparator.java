package com.nespresso.sofa.recruitment.restaurant.preparators;

import com.nespresso.sofa.recruitment.restaurant.models.IngredientQuantity;
import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;
import com.nespresso.sofa.recruitment.restaurant.types.Unit;

public class TomatoMozzarellaSaladPreparator extends Preparator {
	
	public TomatoMozzarellaSaladPreparator(Integer cookingDuration){
		super(cookingDuration);
		this.ingredient.add(new IngredientQuantity(Ingeredient.ballsMozzarella,
				new Quantity(1, Unit.NONE)));
		this.ingredient.add(new IngredientQuantity(Ingeredient.tomatoes,
				new Quantity(2, Unit.NONE)));
		this.ingredient.add(new IngredientQuantity(Ingeredient.oliveOil,
				new Quantity(null, Unit.NONE)));
	}

	@Override
	public Integer cookingDuration(Integer quantity) {
		Integer cookingDurationResutl = (quantity * (cookingDuration / 2));
		cookingDurationResutl += (cookingDuration / 2);
		return cookingDurationResutl;
	}

}
