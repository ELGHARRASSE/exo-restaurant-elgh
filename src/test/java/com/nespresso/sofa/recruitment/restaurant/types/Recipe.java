package com.nespresso.sofa.recruitment.restaurant.types;

import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.preparators.PizzaPreparator;
import com.nespresso.sofa.recruitment.restaurant.preparators.Preparator;
import com.nespresso.sofa.recruitment.restaurant.preparators.TomatoMozzarellaSaladPreparator;

public enum Recipe {

	TomatoMozzarellaSalad("Tomato Mozzarella Salad",
			new TomatoMozzarellaSaladPreparator(6)),

	Pizza("Pizza", new PizzaPreparator(10, 10));

	private String name;
	private Preparator preparator;

	private Recipe(String name, Preparator preparator) {
		this.name = name;
		this.preparator = preparator;
	}

	public static Recipe getRecipeByName(String name) {
		Recipe[] values = Recipe.values();

		for (Recipe recipe : values) {
			if (recipe.name.equals(name)) {
				return recipe;
			}
		}
		throw new IllegalArgumentException();
	}

	public Integer cookingDuration(Integer quantity) {
		return preparator.cookingDuration(quantity);
	}

	public boolean canBeExecuted(Map<Ingeredient, Quantity> stock,
			Integer recipeOrderedQuantity) {
		return preparator.canPrepare(stock, recipeOrderedQuantity);
	}

}
